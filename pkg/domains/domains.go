package domains

type MyJobs struct {
	Jobs []struct {
		Img  string `json:"img"`
		Item []struct {
			Country  string `json:"country"`
			Details  string `json:"details"`
			Duration string `json:"duration"`
			Location string `json:"location"`
			Position string `json:"position"`
			Rank     int    `json:"rank"`
			Title    string `json:"title"`
		} `json:"item"`
		Name string `json:"name"`
		Path string `json:"path"`
	} `json:"jobs"`
	Maintitle string `json:"maintitle"`
	Subtitle  string `json:"subtitle"`
}

type MyKnowlege []struct {
	ID    int    `json:"id"`
	Img   string `json:"img"`
	Items []struct {
		Duration int    `json:"duration"`
		Extra    string `json:"extra"`
		ID       int    `json:"id"`
		Img      string `json:"img"`
		Item     string `json:"item"`
		Link     string `json:"link"`
		Rating   int    `json:"rating"`
	} `json:"items"`
	Link  string `json:"link"`
	Title string `json:"title"`
}